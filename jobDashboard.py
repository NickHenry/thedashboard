from flask import Flask
from pip._vendor import requests
import xml.etree.ElementTree as ET
from datetime import date, timedelta
import time
import gviz_api

app = Flask(__name__)

page_template = """
<html>
  <script src="https://www.google.com/jsapi" type="text/javascript"></script>
  <script>
    google.load('visualization', '1', {packages:['line']});


    function drawTable() {
      %(jscode)s
      var jscode_table = new google.charts.Line(document.getElementById('table_div_jscode'));
      jscode_table.draw(jscode_data);
    }
    google.setOnLoadCallback(drawTable);
  </script>
  <body>
    <div id="table_div_jscode"></div>
  </body>
</html>
"""

@app.route('/')
def currentJob():

    lastweek = date.today() - timedelta(2)
    r = requests.get("https://api.workflowmax.com/time.api/list?apiKey=14C10292983D48CE86E1AA1FE0F8DDFE&accountKey=EE6B492170C94ACD86C880AB29C06E09&from=" + lastweek.strftime('%Y%m%d') + "&to=" + time.strftime('%Y%m%d'))
    root = ET.fromstring(r.text.encode('utf-8'))
    times = root[1]
    timesheets = []
    chartData = []
    dates = []
    jobNames = []
    jobsOnDates = []
    description = {"date": ("string", "Date")}
    for timesheet in times:
        job = {
            "jobId": timesheet.find("Job").find("ID").text,
            "label": timesheet.find("Job").find("Name").text,
            "minutes": timesheet.find("Minutes").text,
            "date": timesheet.find("Date").text
        }
        timesheets.append(job);
    for job in timesheets:
        if job["label"] not in jobNames:
            jobNames.append(job["label"])

        if job["date"] not in dates:
            dates.append(job["date"])
            jobsOnDates.append({"date": job["date"], "jobs": []})

    for datestr in dates:
        for job in timesheets:
            if datestr == job["date"]:
                jobdate = next(obj for obj in jobsOnDates if obj["date"] == datestr)
                jobdate["jobs"].append(job)

    jobsWithTimes = []
    for jobondate in jobsOnDates:
        for job in jobondate["date"]:
            jobId = job["jobId"]
            jobidlist = []
            for obj in jobsWithTimes:
                if jobId not in jobidlist:
                    jobidlist.append(jobId)
                    jobsWithTimes.append(job)
                else:
                    jobminutes = next(obj for obj in jobsWithTimes if obj == datestr)


    print dates
    print jobNames
    print jobsOnDates
    print jobsWithTimes



    data = [
            {"name": "Green Flow Part 1", "minutes": 120, "date": "11/08/2016"},
            {"name": "Green Flow Part 1", "minutes": 130, "date": "11/09/2016"},
            {"name": "Green Flow Part 1", "minutes": 100, "date": "11/10/2016"},
            {"name": "Green Flow Part 4", "minutes": 150, "date": "11/11/2016"},
            {"name": "Green Flow Part 5", "minutes": 34, "date": "11/12/2016"}
        ]

    data_table = gviz_api.DataTable(description)
    data_table.LoadData(data)

    jscode = data_table.ToJSCode("jscode_data", columns_order=("date", "minutes"), order_by="date")

    return page_template % vars()

if __name__ == '__main__':
    app.run()
